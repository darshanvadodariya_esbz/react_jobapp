import "./App.css";
import { useState } from "react";
import BasicDetails from "./components/basicDetails";
import EducationDetail from "./components/educationDetail";
import WorkExperience from "./components/workExperience";
import LanguageKnown from "./components/languageKnown";
import TechnologiesKnown from "./components/technologiesKnown";
import ReferenceContact from "./components/referenceContact";
import Preferences from "./components/preferences";

function App() {
  const [step, setstep] = useState(1);
  const [errors] = useState({});

  //state for form data
  const [formData, setFormData] = useState({
    firstName: "",
    lastName: "",
    email: "",
    designation: "",
    phone: "",
    dob: "",
    add1: "",
    add2: "",
    country: "",
    state: "",
    city: "",
    zip: "",
    gender: "",
    relStatus: "",
    education: [],
    workExp: [],
    langKnow: [],
    techKnow: [],
    refCont: [],
    notice_period: "",
    expected_ctc: "",
    current_ctc: "",
    preferd_loc: [],
    department: "",
  });

  const makeRequest = (formData) => {
    alert("Success!!");
    console.log("Form Submitted", formData);
  };

  const nextStep = (newData) => {
    setFormData((prev) => ({ ...prev, ...newData }));
    setstep((prev) => prev + 1);
    console.log("data", newData);
  };

  const prevStep = (newData) => {
    setFormData((prev) => ({ ...prev, ...newData }));
    setstep((prev) => prev - 1);
  };

  const lastStep = (newData, final = false) => {
    setFormData((prev) => ({ ...prev, ...newData }));
    if (final) {
      makeRequest(newData);
      return;
    }
  };

  switch (step) {
    case 1:
      return (
        <div className="App">
          <BasicDetails nextStep={nextStep} data={formData} errors={errors} />
        </div>
      );
    case 2:
      return (
        <div className="App">
          <EducationDetail
            nextStep={nextStep}
            prevStep={prevStep}
            data={formData}
          />
        </div>
      );
    case 3:
      return (
        <div className="App">
          <WorkExperience
            nextStep={nextStep}
            prevStep={prevStep}
            data={formData}
          />
        </div>
      );
    case 4:
      return (
        <div className="App">
          <LanguageKnown
            nextStep={nextStep}
            prevStep={prevStep}
            data={formData}
          />
        </div>
      );
    case 5:
      return (
        <div className="App">
          <TechnologiesKnown
            nextStep={nextStep}
            prevStep={prevStep}
            data={formData}
          />
        </div>
      );
    case 6:
      return (
        <div className="App">
          <ReferenceContact
            nextStep={nextStep}
            prevStep={prevStep}
            data={formData}
          />
        </div>
      );
    case 7:
      return (
        <div className="App">
          <Preferences
            nextStep={lastStep}
            prevStep={prevStep}
            data={formData}
          />
        </div>
      );
    default:
      return <div className="App"></div>;
  }
}

export default App;
