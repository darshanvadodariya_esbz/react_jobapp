import { Field, FieldArray } from "formik";

const WorkExperience = ({ formik, isboolvalue, setisbutton }) => {
  const { errors, touched } = formik;
  if (touched.workExp) {
    setisbutton(true);
  }

  return (
    <div>
      <div className="text-danger text-center">
        {typeof errors.workExp === "string"
          ? "Must have at least Work Experience Details add"
          : null}
      </div>
      <FieldArray
        name="workExp"
        render={(arrayHelpers) => {
          const { form } = arrayHelpers;
          const { values } = form;
          const { workExp } = values;
          return (
            <div>
              <div className="mb-3 mt-3 col-11 d-flex justify-content-end">
                <button
                  type="button"
                  className="btn btn-outline-primary"
                  onClick={() =>
                    arrayHelpers.push({
                      companyName: "",
                      work_designation: "",
                      from_date: "",
                      to_date: "",
                    })
                  }
                >
                  Add
                </button>
              </div>
              {workExp && workExp.length > 0
                ? workExp.map((work, index) => (
                    <div key={index}>
                      <div className="row mb-3">
                        <div className="form-group col-2">
                          <label>Company Name</label>
                          <Field
                            name={`workExp.${index}.companyName`}
                            type="text"
                            className="form-control"
                          />
                          {isboolvalue &&
                            errors?.workExp?.[index]?.companyName && (
                              <div className="text-danger">
                                {errors.workExp[index].companyName}
                              </div>
                            )}
                        </div>
                        <div className="form-group col-2">
                          <label>Work Designation</label>
                          <Field
                            name={`workExp.${index}.work_designation`}
                            type="text"
                            className="form-control"
                          />
                          {isboolvalue &&
                            errors?.workExp?.[index]?.work_designation && (
                              <div className="text-danger">
                                {errors.workExp[index].work_designation}
                              </div>
                            )}
                        </div>
                        <div className="form-group col-2">
                          <label>From Date</label>
                          <Field
                            name={`workExp.${index}.from_date`}
                            type="date"
                            className="form-control"
                          />
                          {isboolvalue &&
                            errors?.workExp?.[index]?.from_date && (
                              <div className="text-danger">
                                {errors.workExp[index].from_date}
                              </div>
                            )}
                        </div>
                        <div className="form-group col-2">
                          <label>To Date</label>
                          <Field
                            name={`workExp.${index}.to_date`}
                            type="date"
                            className="form-control"
                          />
                          {isboolvalue &&
                            errors?.workExp?.[index]?.to_date && (
                              <div className="text-danger">
                                {errors.workExp[index].to_date}
                              </div>
                            )}
                        </div>
                        <div className="form-group col-2">
                          <label>Action</label>
                          <br />
                          <button
                            type="button"
                            className="btn btn-outline-danger"
                            onClick={() => arrayHelpers.remove(index)}
                          >
                            Remove
                          </button>
                        </div>
                      </div>
                      <br />
                      <br />
                    </div>
                  ))
                : null}
            </div>
          );
        }}
      />
    </div>
  );
};

export default WorkExperience;
