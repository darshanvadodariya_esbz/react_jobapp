import { Field, FieldArray } from "formik";

const LanguageKnown = ({ formik, isboolvalue, setisbutton }) => {
  const { errors, touched } = formik;
  if (touched.langKnow) {
    setisbutton(true);
  }

  const langList = [
    { id: "hindi", name: "Hindi" },
    { id: "english", name: "English" },
    { id: "gujarati", name: "Gujarati" },
  ];

  return (
    <div>
      <div className="text-danger text-center">
        {typeof errors.langKnow === "string"
          ? "Must have at least Language Details add"
          : null}
      </div>
      <FieldArray
        name="langKnow"
        render={(arrayHelpers) => {
          const { form } = arrayHelpers;
          const { values } = form;
          const { langKnow } = values;
          return (
            <div>
              <div className="mb-3 mt-3 col-11 d-flex justify-content-end">
                <button
                  type="button"
                  className="btn btn-outline-primary"
                  onClick={() =>
                    arrayHelpers.push({
                      known: "",
                      status: [],
                    })
                  }
                >
                  Add
                </button>
              </div>
              {langKnow && langKnow.length > 0
                ? langKnow.map((lang, index) => (
                    <div key={index}>
                      <div className="row mb-3">
                        <div className="form-group col-4">
                          <label>Language known</label>
                          <Field
                            name={`langKnow.${index}.known`}
                            className="form-select"
                            as="select"
                          >
                            <option value="" disabled>
                              Select Result
                            </option>
                            {langList.length > 0 &&
                              langList.map((item, i) => (
                                <option
                                  key={i}
                                  value={item.id}
                                  hidden={langKnow.find(
                                    (langdata) => langdata.known === item.id
                                  )}
                                >
                                  {item.name}
                                </option>
                              ))}
                          </Field>
                          {isboolvalue &&
                            errors?.langKnow?.[index]?.known && (
                              <div className="text-danger">
                                {errors.langKnow[index].known}
                              </div>
                            )}
                        </div>

                        <div className="form-group col-4">
                          <label>known</label>
                          <div
                            className="d-flex flex-wrap justify-content-between mb-3 mt-2"
                            role="group"
                            aria-labelledby="checkbox-group"
                          >
                            <label>
                              <Field
                                type="checkbox"
                                name={`langKnow.${index}.status`}
                                value="read"
                              />
                              read
                            </label>
                            <label>
                              <Field
                                type="checkbox"
                                name={`langKnow.${index}.status`}
                                value="write"
                              />
                              write
                            </label>
                            <label>
                              <Field
                                type="checkbox"
                                name={`langKnow.${index}.status`}
                                value="speak"
                              />
                              speak
                            </label>
                          </div>
                          {isboolvalue &&
                            errors?.langKnow?.[index]?.status && (
                              <div className="text-danger">
                                {errors.langKnow[index].status}
                              </div>
                            )}
                        </div>

                        <div className="form-group col-4">
                          <label>Action</label>
                          <br />
                          <button
                            type="button"
                            className="btn btn-outline-danger"
                            onClick={() => arrayHelpers.remove(index)}
                          >
                            Remove
                          </button>
                        </div>
                      </div>
                    </div>
                  ))
                : null}
            </div>
          );
        }}
      />
    </div>
  );
};

export default LanguageKnown;
