import { Field, FieldArray } from "formik";

const ReferenceContact = ({ formik, isboolvalue, setisbutton }) => {
  const { errors, touched } = formik;
  if (touched.refCont) {
    setisbutton(true);
  }

  return (
    <div>
      <div className="text-danger text-center">
        {typeof errors.refCont === "string"
          ? "Must have at least Reference Details add"
          : null}
      </div>
      <FieldArray
        name="refCont"
        render={(arrayHelpers) => {
          const { form } = arrayHelpers;
          const { values } = form;
          const { refCont } = values;
          return (
            <div>
              <div className="mb-3 mt-3 col-11 d-flex justify-content-end">
                <button
                  type="button"
                  className="btn btn-outline-primary"
                  onClick={() =>
                    arrayHelpers.push({
                      ref_name: "",
                      ref_number: "",
                      ref_relation: "",
                    })
                  }
                >
                  Add
                </button>
              </div>
              {refCont && refCont.length > 0
                ? refCont.map((ref, index) => (
                    <div key={index}>
                      <div className="row mb-3">
                        <div className="form-group col-3">
                          <label>Reference Name</label>
                          <Field
                            name={`refCont.${index}.ref_name`}
                            type="text"
                            className="form-control"
                          />
                          {isboolvalue &&
                            errors?.refCont?.[index]?.ref_name && (
                              <div className="text-danger">
                                {errors.refCont[index].ref_name}
                              </div>
                            )}
                        </div>
                        <div className="form-group col-3">
                          <label>Reference Number</label>
                          <Field
                            name={`refCont.${index}.ref_number`}
                            type="text"
                            className="form-control"
                          />
                          {isboolvalue &&
                            errors?.refCont?.[index]?.ref_number && (
                              <div className="text-danger">
                                {errors.refCont[index].ref_number}
                              </div>
                            )}
                        </div>
                        <div className="form-group col-3">
                          <label>Reference Relation</label>
                          <Field
                            name={`refCont.${index}.ref_relation`}
                            type="text"
                            className="form-control"
                          />
                          {isboolvalue &&
                            errors?.refCont?.[index]?.ref_relation && (
                              <div className="text-danger">
                                {errors.refCont[index].ref_relation}
                              </div>
                            )}
                        </div>
                        <div className="form-group col-3">
                          <label>Action</label>
                          <br />
                          <button
                            type="button"
                            className="btn btn-outline-danger"
                            onClick={() => arrayHelpers.remove(index)}
                          >
                            Remove
                          </button>
                        </div>
                      </div>
                      <br />
                      <br />
                    </div>
                  ))
                : null}
            </div>
          );
        }}
      />
    </div>
  );
};

export default ReferenceContact;
