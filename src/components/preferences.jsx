import { Field } from "formik";

const Preferences = ({ formik, isboolvalue, setisbutton }) => {
  const { errors, touched } = formik;
  if (Object.keys(touched).length !== 0) {
    setisbutton(true);
  }

  return (
    <div>
      <div className="row mb-3">
        <div className="form-group col-4">
          <label>Notice Period</label>
          <Field name="notice_period" type="text" className="form-control" />
          {errors.notice_period &&
              isboolvalue && (<div className="text-danger">{errors.notice_period}</div>)}
        </div>

        <div className="form-group col-4">
          <label>Expected CTC</label>
          <Field name="expected_ctc" type="text" className="form-control" />
          {errors.expected_ctc &&
              isboolvalue && (<div className="text-danger">{errors.expected_ctc}</div>)}
        </div>

        <div className="form-group col-4">
          <label>Current CTC</label>
          <Field name="current_ctc" type="text" className="form-control" />
          {errors.current_ctc &&
              isboolvalue && (<div className="text-danger">{errors.current_ctc}</div>)}
        </div>
      </div>

      <div className="row mb-3">
        <div className="form-group col-4">
          <label>Preferred Location</label>
          <Field
            name="preferd_loc"
            className="form-select"
            as="select"
            multiple
          >
            <option value="" disabled>
              Select location
            </option>
            <option value="ahmedabad">Ahmedabad</option>
            <option value="bhavnagar">Bhavnagar</option>
            <option value="rajkot">Rajkot</option>
            <option value="vadodara">Vadodara</option>
          </Field>
          {errors.preferd_loc &&
              isboolvalue && (<div className="text-danger">{errors.preferd_loc}</div>)}
        </div>

        <div className="form-group col-4">
          <label>Department</label>
          <Field name="department" className="form-select" as="select">
            <option value="">Select department</option>
            <option value="design">Design</option>
            <option value="marketing">marketing</option>
          </Field>
          {errors.department &&
              isboolvalue && (<div className="text-danger">{errors.department}</div>)}
        </div>
      </div>
    </div>
  );
};

export default Preferences;
