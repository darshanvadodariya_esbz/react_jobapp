import { Field, FieldArray } from "formik";

const EducationDetail = ({ formik, isboolvalue, setisbutton }) => {
  const resultList = [
    { id: "ssc", name: "SSC" },
    { id: "hsc", name: "HSC/Diploma" },
    { id: "bachelor", name: "Bachelor Degree" },
    { id: "master", name: "Master Degree" },
  ];
  
  const { errors, touched } = formik;
  if (touched.education) {
    setisbutton(true);
  }
  return (
    <div>
      <div className="text-danger text-center">
        {typeof errors.education === "string"
          ? "Must have at least Education Details add"
          : null}
      </div>
      <FieldArray
        name="education"
        render={(arrayHelpers) => {
          const { form } = arrayHelpers;
          const { values } = form;
          const { education } = values;
          return (
            <div>
              <div className="mb-3 mt-3 col-11 d-flex justify-content-end">
                <button
                  type="button"
                  className="btn btn-outline-primary"
                  onClick={() =>
                    arrayHelpers.push({
                      result: "",
                      board: "",
                      school_university: "",
                      passing_year: "",
                      percentage: "",
                    })
                  }
                >
                  Add
                </button>
              </div>
              {education &&
                education.length > 0 ?
                education.map((edu, index) => (
                  <div key={index}>
                    <div className="row mb-3">
                      <div className="form-group col-2">
                        <label>Result</label>
                        <Field
                          name={`education.${index}.result`}
                          className="form-select"
                          as="select"
                        >
                          <option value="" disabled>
                            Select Result
                          </option>
                          {resultList.length > 0 &&
                            resultList.map((item, i) => (
                              <option
                                key={i}
                                value={item.id}
                                hidden={education.find(
                                  (edudata) => edudata.result === item.id
                                )}
                              >
                                {item.name}
                              </option>
                            ))}
                        </Field>
                        {errors?.education?.[index]?.result && isboolvalue && (
                          <div className="text-danger">
                            {errors.education[index].result}
                          </div>
                        )}
                      </div>

                      <div className="form-group col-2">
                        <label>Board</label>
                        <Field
                          name={`education.${index}.board`}
                          type="text"
                          className="form-control"
                        />
                        {errors?.education?.[index]?.board && isboolvalue && (
                          <div className="text-danger">
                            {errors.education[index].board}
                          </div>
                        )}
                      </div>
                      <div className="form-group col-2">
                        <label>School/University</label>
                        <Field
                          name={`education.${index}.school_university`}
                          type="text"
                          className="form-control"
                        />
                        {errors?.education?.[index]?.school_university &&
                          isboolvalue && (
                            <div className="text-danger">
                              {errors.education[index].school_university}
                            </div>
                          )}
                      </div>
                      <div className="form-group col-2">
                        <label>Passing Year</label>
                        <Field
                          name={`education.${index}.passing_year`}
                          type="text"
                          className="form-control"
                        />
                        {errors?.education?.[index]?.passing_year &&
                          isboolvalue && (
                            <div className="text-danger">
                              {errors.education[index].passing_year}
                            </div>
                          )}
                      </div>
                      <div className="form-group col-2">
                        <label>Percentage</label>
                        <Field
                          name={`education.${index}.percentage`}
                          type="text"
                          className="form-control"
                        />
                        {errors?.education?.[index]?.percentage &&
                          isboolvalue && (
                            <div className="text-danger">
                              {errors.education[index].percentage}
                            </div>
                          )}
                      </div>
                      <div className="form-group col-2">
                        <label>Action</label>
                        <br />
                        <button
                            className="btn btn-outline-danger"
                            type="button"
                            onClick={() => arrayHelpers.remove(index)}
                          >
                            remove
                          </button>
                      </div>
                    </div>
                    <br />
                    <br />
                  </div>
                )): null}
            </div>
          );
        }}
      />
    </div>
  );
};

export default EducationDetail;
