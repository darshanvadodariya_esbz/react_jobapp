import {Field } from "formik";

const BasicDetails = ({formik, isboolvalue,setisbutton}) => {
  const { errors, touched } = formik;
  if(Object.keys(touched).length!==0){
    setisbutton(true);
  }
  return (
    <div>
      <div className="row mb-3">
        <div className="form-group col-4">
          <label>First Name</label>
          <Field name="firstName" type="text" className="form-control" />
          {errors.firstName &&
              isboolvalue && (<div className="text-danger">{errors.firstName}</div>)}
        </div>

        <div className="form-group col-4">
          <label>Last Name</label>
          <Field name="lastName" type="text" className="form-control" />
          {errors.lastName &&
              isboolvalue && (<div className="text-danger">{errors.lastName}</div>)}
        </div>
        <div className="form-group col-4">
          <label>Email</label>
          <Field name="email" type="text" className="form-control" />
          {errors.email &&
              isboolvalue && (<div className="text-danger">{errors.email}</div>)}
        </div>
      </div>

      <div className="row mb-3">
        <div className="form-group col-4">
          <label>Designation</label>
          <Field name="designation" type="text" className="form-control" />
          {errors.designation &&
              isboolvalue && (<div className="text-danger">{errors.designation}</div>)}
        </div>
        <div className="form-group col-4">
          <label>Phone</label>
          <Field name="phone" type="text" className="form-control" />
          {errors.phone &&
              isboolvalue && (<div className="text-danger">{errors.phone}</div>)}
        </div>

        <div className="form-group col-4">
          <label>Date of Birth</label>
          <Field name="dob" type="date" className="form-control" />
          {errors.dob &&
              isboolvalue && (<div className="text-danger">{errors.dob}</div>)}
        </div>
      </div>
      <div className="row mb-3">
        <div className="form-group col-6">
          <label>Address 1</label>
          <Field name="add1" type="text" className="form-control" />
          {errors.add1 &&
              isboolvalue && (<div className="text-danger">{errors.add1}</div>)}
        </div>

        <div className="form-group col-6">
          <label>Address 2</label>
          <Field name="add2" type="text" className="form-control" />
          {errors.add2 &&
              isboolvalue && (<div className="text-danger">{errors.add2}</div>)}
        </div>
      </div>

      <div className="row mb-3">
        <div className="form-group col-4">
          <label>Country</label>
          <Field name="country" className="form-select" as="select">
            <option value="">Select Country</option>
            <option value="brazil">Brazil</option>
            <option value="canada">Canada</option>
            <option value="germany">Germany</option>
            <option value="india">India</option>
          </Field>
          {errors.country &&
              isboolvalue && (<div className="text-danger">{errors.country}</div>)}
        </div>

        <div className="form-group col-4">
          <label>State</label>
          <Field name="state" className="form-select" as="select">
            <option value="">Select State</option>
            <option value="gujarat">Gujarat</option>
            <option value="rajasthan">Rajasthan</option>
            <option value="punjab">Punjab</option>
            <option value="assam">Assam</option>
          </Field>
          {errors.state &&
              isboolvalue && (<div className="text-danger">{errors.state}</div>)}
        </div>
        <div className="form-group col-4">
          <label>City</label>
          <Field name="city" className="form-select" as="select">
            <option value="">Select City</option>
            <option value="rajkot">Rajkot</option>
            <option value="ahmedabad">Ahmedabad</option>
            <option value="bhavnagar">Bhavnagar</option>
            <option value="abu">Abu</option>
          </Field>
          {errors.city &&
              isboolvalue && (<div className="text-danger">{errors.city}</div>)}
        </div>
      </div>

      <div className="row mb-3">
        <div className="form-group col-4">
          <label>Zip code</label>
          <Field name="zip" type="text" className="form-control" />
          {errors.zip &&
              isboolvalue && (<div className="text-danger">{errors.zip}</div>)}
        </div>
        <div className="form-group col-4">
          <label>Gender</label>
          <div
            role="group"
            className="mb-3 mt-2"
            aria-labelledby="my-radio-group"
          >
            <label>
              <Field type="radio" name="gender" value="male" />
              Male
            </label>
            <label>
              <Field type="radio" name="gender" value="female" />
              Female
            </label>
          </div>
          {errors.gender &&
              isboolvalue && (<div className="text-danger">{errors.gender}</div>)}
        </div>

        <div className="form-group col-4">
          <label>RelationShip Status</label>
          <Field name="relStatus" type="text" className="form-control" />
          {errors.relStatus &&
              isboolvalue && (<div className="text-danger">{errors.relStatus}</div>)}
        </div>
      </div>
    </div>
  );
};

export default BasicDetails;
