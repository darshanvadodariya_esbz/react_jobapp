import { Field, FieldArray } from "formik";

const TechnologiesKnown = ({ formik, isboolvalue, setisbutton }) => {
  const { errors, touched } = formik;
  if (touched.techKnow) {
    setisbutton(true);
  }

  const techList = [
    { id: "php", name: "php" },
    { id: "mysql", name: "MySQL" },
    { id: "laravel", name: "Laravel" },
    { id: "oracle", name: "Oracle" },
  ];

  return (
    <div>
      <div className="text-danger text-center">
        {typeof errors.techKnow === "string"
          ? "Must have at least Technology Details add"
          : null}
      </div>
      <FieldArray
        name="techKnow"
        render={(arrayHelpers) => {
          const { form } = arrayHelpers;
          const { values } = form;
          const { techKnow } = values;
          return (
            <div>
              <div className="mb-3 mt-3 col-11 d-flex justify-content-end">
                <button
                  type="button"
                  className="btn btn-outline-primary"
                  onClick={() =>
                    arrayHelpers.push({
                      known: "",
                      status: "",
                    })
                  }
                >
                  Add
                </button>
              </div>
              {techKnow && techKnow.length > 0
                ? techKnow.map((tech, index) => (
                    <div key={index}>
                      <div className="row mb-3">
                        <div className="form-group col-4">
                          <label>Technologies known</label>
                          <Field
                            name={`techKnow.${index}.known`}
                            className="form-select"
                            as="select"
                          >
                            <option value="" disabled>
                              Select Result
                            </option>
                            {techList.length > 0 &&
                              techList.map((item, i) => (
                                <option
                                  key={i}
                                  value={item.id}
                                  hidden={techKnow.find(
                                    (techdata) => techdata.known === item.id
                                  )}
                                >
                                  {item.name}
                                </option>
                              ))}
                          </Field>
                          {isboolvalue &&
                            errors?.techKnow?.[index]?.known && (
                              <div className="text-danger">
                                {errors.techKnow[index].known}
                              </div>
                            )}
                        </div>

                        <div className="form-group col-4">
                          <label>Status</label>
                          <div
                            className="d-flex flex-wrap justify-content-between mb-3 mt-2"
                            role="group"
                            aria-labelledby="my-radio-group"
                          >
                            <label>
                              <Field
                                type="radio"
                                name={`techKnow.${index}.status`}
                                value="beginer"
                              />
                              beginer
                            </label>
                            <label>
                              <Field
                                type="radio"
                                name={`techKnow.${index}.status`}
                                value="mideator"
                              />
                              mideator
                            </label>
                            <label>
                              <Field
                                type="radio"
                                name={`techKnow.${index}.status`}
                                value="expert"
                              />
                              expert
                            </label>
                          </div>
                          {isboolvalue &&
                            errors?.techKnow?.[index]?.status && (
                              <div className="text-danger">
                                {errors.techKnow[index].status}
                              </div>
                            )}
                        </div>

                        <div className="form-group col-4">
                          <label>Action</label>
                          <br />
                          <button
                            type="button"
                            className="btn btn-outline-danger"
                            onClick={() => arrayHelpers.remove(index)}
                          >
                            Remove
                          </button>
                        </div>
                      </div>
                    </div>
                  ))
                : null}
            </div>
          );
        }}
      />
    </div>
  );
};

export default TechnologiesKnown;
