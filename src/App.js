import "./App.css";
import JobAppForm from "./jobAppForm/jobAppForm";

function App() {
  return (
    <div className="App">
     <JobAppForm/>
    </div>
  );
}

export default App;
