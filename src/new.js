import { useState } from "react";
import { Card } from "react-bootstrap";
// import "./styles.css";
import { Formik, Form, Field, ErrorMessage, FieldArray } from "formik";
import * as Yup from "yup";

export default function App() {
  const [data, setData] = useState({
    firstName: "",
    lastName: "",
    email: "",
    designation: "",
    phone: "",
    dob: "",
    add1: "",
    add2: "",
    country: "",
    state: "",
    city: "",
    zip: "",
    gender: "",
    relStatus: "",
    education: [],
    workExp: [],
    langKnow: [],
    techKnow: [],
    refCont: [],
    notice_period: "",
    expected_ctc: "",
    current_ctc: "",
    preferd_loc: [],
    department: "",
  });
  const [currentStep, setCurrentStep] = useState(0);
  const [errors] = useState({});

  const makeRequest = (formData) => {
    console.log("Form Submitted", formData);
  };

  const handleLastStep = (newData, final = false) => {
    setData((prev) => ({ ...prev, ...newData }));

    if (final) {
      makeRequest(newData);
      return;
    }
  };

  const handleNextStep = (newData) => {
    setData((prev) => ({ ...prev, ...newData }));
    setCurrentStep((prev) => prev + 1);
    console.log("data", newData);
  };

  const handlePrevStep = (newData) => {
    setData((prev) => ({ ...prev, ...newData }));
    setCurrentStep((prev) => prev - 1);
  };

  const steps = [
    <StepOne next={handleNextStep} data={data} errors={errors} />,
    <StepTwo next={handleNextStep} prev={handlePrevStep} data={data} />,
    <StepThree next={handleNextStep} prev={handlePrevStep} data={data} />,
    <StepFour next={handleNextStep} prev={handlePrevStep} data={data} />,
    <StepFive next={handleNextStep} prev={handlePrevStep} data={data} />,
    <StepSix next={handleNextStep} prev={handlePrevStep} data={data} />,
    <StepSeven next={handleLastStep} prev={handlePrevStep} data={data} />,
  ];

  

  return <div className="App">{steps[currentStep]}</div>;
}
// Basic Detail
const stepOneValidationSchema = Yup.object({
  firstName: Yup.string().required(),
  lastName: Yup.string().required(),
  email: Yup.string().email("Invalid email").required("Required"),
  designation: Yup.string().required(),
  phone: Yup.string()
    .matches(/^[0-9\b]+$/, "allow only number")
    .length(10)
    .required(),
  dob: Yup.string().required("Date of Birth is Required"),
  add1: Yup.string().required("Address Is Required"),
  add2: Yup.string().nullable(true),
  country: Yup.string().required("Select Country"),
  state: Yup.string().required("Select State"),
  city: Yup.string().required("Select City"),
  zip: Yup.string()
    .matches(/^[0-9\b]+$/, "allow only number")
    .length(6)
    .required(),
  gender: Yup.string().required("Select Gender"),
  relStatus: Yup.string().required("RelationShip Status is a required"),
});

const StepOne = (props) => {
  const handleSubmit = (values) => {
    props.next(values);
  };

  return (
    <div className="container">
      <Card style={{ marginTop: 50 }}>
        <Card.Body>
          <Formik
            // validationSchema={stepOneValidationSchema}
            initialValues={props.data}
            onSubmit={handleSubmit}
          >
            {() => (
              <Form>
                <div className="row mb-3">
                  <div className="form-group col-4">
                    <label>First Name</label>
                    <Field
                      name="firstName"
                      type="text"
                      className="form-control"
                    />
                    <ErrorMessage
                      name="firstName"
                      component="div"
                      className="text-danger"
                    />
                  </div>

                  <div className="form-group col-4">
                    <label>Last Name</label>
                    <Field
                      name="lastName"
                      type="text"
                      className="form-control"
                    />
                    <ErrorMessage
                      name="lastName"
                      component="div"
                      className="text-danger"
                    />
                  </div>
                  <div className="form-group col-4">
                    <label>Email</label>
                    <Field name="email" type="text" className="form-control" />
                    <ErrorMessage
                      name="email"
                      component="div"
                      className="text-danger"
                    />
                  </div>
                </div>

                <div className="row mb-3">
                  <div className="form-group col-4">
                    <label>Designation</label>
                    <Field
                      name="designation"
                      type="text"
                      className="form-control"
                    />
                    <ErrorMessage
                      name="designation"
                      component="div"
                      className="text-danger"
                    />
                  </div>
                  <div className="form-group col-4">
                    <label>Phone</label>
                    <Field name="phone" type="text" className="form-control" />
                    <ErrorMessage
                      name="phone"
                      component="div"
                      className="text-danger"
                    />
                  </div>

                  <div className="form-group col-4">
                    <label>Date of Birth</label>
                    <Field name="dob" type="date" className="form-control" />
                    <ErrorMessage
                      name="dob"
                      component="div"
                      className="text-danger"
                    />
                  </div>
                </div>
                <div className="row mb-3">
                  <div className="form-group col-6">
                    <label>Address 1</label>
                    <Field name="add1" type="text" className="form-control" />
                    <ErrorMessage
                      name="add1"
                      component="div"
                      className="text-danger"
                    />
                  </div>

                  <div className="form-group col-6">
                    <label>Address 2</label>
                    <Field name="add2" type="text" className="form-control" />
                    <ErrorMessage
                      name="add2"
                      component="div"
                      className="text-danger"
                    />
                  </div>
                </div>

                <div className="row mb-3">
                  <div className="form-group col-4">
                    <label>Country</label>
                    <Field name="country" className="form-select" as="select">
                      <option value="">Select Country</option>
                      <option value="brazil">Brazil</option>
                      <option value="canada">Canada</option>
                      <option value="germany">Germany</option>
                      <option value="india">India</option>
                    </Field>
                    <ErrorMessage
                      name="country"
                      component="div"
                      className="text-danger"
                    />
                  </div>

                  <div className="form-group col-4">
                    <label>State</label>
                    <Field name="state" className="form-select" as="select">
                      <option value="">Select State</option>
                      <option value="gujarat">Gujarat</option>
                      <option value="rajasthan">Rajasthan</option>
                      <option value="punjab">Punjab</option>
                      <option value="assam">Assam</option>
                    </Field>
                    <ErrorMessage
                      name="state"
                      component="div"
                      className="text-danger"
                    />
                  </div>
                  <div className="form-group col-4">
                    <label>City</label>
                    <Field name="city" className="form-select" as="select">
                      <option value="">Select City</option>
                      <option value="rajkot">Rajkot</option>
                      <option value="ahmedabad">Ahmedabad</option>
                      <option value="bhavnagar">Bhavnagar</option>
                      <option value="abu">Abu</option>
                    </Field>
                    <ErrorMessage
                      name="city"
                      component="div"
                      className="text-danger"
                    />
                  </div>
                </div>

                <div className="row mb-3">
                  <div className="form-group col-4">
                    <label>Zip code</label>
                    <Field name="zip" type="text" className="form-control" />
                    <ErrorMessage
                      name="zip"
                      component="div"
                      className="text-danger"
                    />
                  </div>
                  <div className="form-group col-4">
                    <label>Gender</label>
                    <div
                      role="group"
                      className="mb-3 mt-2"
                      aria-labelledby="my-radio-group"
                    >
                      <label>
                        <Field type="radio" name="gender" value="male" />
                        Male
                      </label>
                      <label>
                        <Field type="radio" name="gender" value="female" />
                        Female
                      </label>
                    </div>
                    <ErrorMessage
                      name="gender"
                      component="div"
                      className="text-danger"
                    />
                  </div>

                  <div className="form-group col-4">
                    <label>RelationShip Status</label>
                    <Field
                      name="relStatus"
                      type="text"
                      className="form-control"
                    />
                    <ErrorMessage
                      name="relStatus"
                      component="div"
                      className="text-danger"
                    />
                  </div>
                </div>
                <hr />
                <div
                  className="d-flex flex-wrap justify-content-evenly mb-3 mt-3"
                  role="group"
                  aria-label="Basic example"
                >
                  <button type="submit" className="btn btn-outline-primary">
                    Next
                  </button>
                </div>
              </Form>
            )}
          </Formik>
        </Card.Body>
      </Card>
    </div>
  );
};

// Education Detail
const stepTwoValidationSchema = Yup.object({
  education: Yup.array()
    .of(
      Yup.object().shape({
        result: Yup.string().required("Select Result"),
        board: Yup.string().required("Board is a required"),
        school_university: Yup.string().required(
          "school / university is a required"
        ),
        passing_year: Yup.string()
          .matches(/^[0-9\b]+$/, "allow only number")
          .min(2, "Passing Year must be at least 2 characters")
          .max(4, "Passing Year must be at least 4 characters")
          .required("Passing Year is a required"),
        percentage: Yup.string()
          .matches(/^[0-9\b]+$/, "allow only number")
          .max(3, "percentage must be at least 3 characters")
          .required("Passing Year is a required")
          .required("Percentage is a required"),
      })
    )
    .min(1, "Must have at least Education Details add.")
    .required(),
});



const StepTwo = (props) => {
  const handleSubmit = (values) => {
    props.next(values, true);
  };

  const item = ["ssc", "hsc", "bachelor", "master" ]

  return (
    <div className="container">
      <Card style={{ marginTop: 50 }}>
        <Card.Body>
          

          <Formik
            validationSchema={stepTwoValidationSchema}
            initialValues={props.data}
            onSubmit={handleSubmit}
          >
            {({ values, errors }) => (
              <Form>
                <div className="text-danger text-center">
                  {typeof errors.education === "string"
                    ? "Must have at least Education Details add"
                    : null}
                </div>
                <FieldArray
                  name="education"
                  render={(arrayHelpers) => {
                    const education = values.education;
                    return (
                      <div>
                        <div className="mb-3 mt-3 col-11 d-flex justify-content-end">
                          <button
                            type="button"
                            className="btn btn-outline-primary"
                            onClick={() =>
                              arrayHelpers.push({
                                result: "",
                                board: "",
                                school_university: "",
                                passing_year: "",
                                percentage: "",
                              })
                            }
                          >
                            Add
                          </button>
                        </div>
                        {education && education.length > 0
                          ? education.map((edu, index) => (
                              <div key={index}>
                                <div className="row mb-3">
                                  <div className="form-group col-2">
                                    <label>Result</label>
                                    <Field
                                      name={`education.${index}.result`}
                                      className="form-select"
                                      as="select"
                                    >
                                      <option hidden={values.education.find((item)=>item.result === "") !== -1 }>Select Result</option>
                                      <option hidden={values.education.find((item)=>item.result === "ssc") !== -1 } key="ssc">SSC</option>
                                      <option hidden={values.education.find((item)=>item.result === "hsc") !== -1 } key="hsc">HSC/Diploma</option>
                                      <option hidden={values.education.find((item)=>item.result === "bachelor") !== -1 } key="bachelor">Bachelor Degree</option>
                                      <option hidden={values.education.find((item)=>item.result === "master") !== -1 } key="master">Master Degree</option>
                                      {/* <option value="hsc" disabled={values.education.find((item)=>item.result === "hsc") !== -1 } key="hsc">HSC/Diploma</option> */}
                                      {/* <option value="ssc">SSC</option> */}
                                      {/* <option value="hsc">HSC/Diploma</option> */}
                                      {/* <option value="bachelor">
                                        Bachelor Degree
                                      </option>
                                      <option value="master">
                                        Master Degree
                                      </option> */}
                                    </Field>
                                    <ErrorMessage
                                      name={`education.${index}.result`}
                                      component="div"
                                      className="text-danger"
                                    />
                                  </div>

                                  <div className="form-group col-2">
                                    <label>Board</label>
                                    <Field
                                      name={`education.${index}.board`}
                                      type="text"
                                      className="form-control"
                                    />
                                    <ErrorMessage
                                      name={`education.${index}.board`}
                                      component="div"
                                      className="text-danger"
                                    />
                                  </div>
                                  <div className="form-group col-2">
                                    <label>School/University</label>
                                    <Field
                                      name={`education.${index}.school_university`}
                                      type="text"
                                      className="form-control"
                                    />
                                    <ErrorMessage
                                      name={`education.${index}.school_university`}
                                      component="div"
                                      className="text-danger"
                                    />
                                  </div>
                                  <div className="form-group col-2">
                                    <label>Passing Year</label>
                                    <Field
                                      name={`education.${index}.passing_year`}
                                      type="text"
                                      className="form-control"
                                    />
                                    <ErrorMessage
                                      name={`education.${index}.passing_year`}
                                      component="div"
                                      className="text-danger"
                                    />
                                  </div>
                                  <div className="form-group col-2">
                                    <label>Percentage</label>
                                    <Field
                                      name={`education.${index}.percentage`}
                                      type="text"
                                      className="form-control"
                                    />
                                    <ErrorMessage
                                      name={`education.${index}.percentage`}
                                      component="div"
                                      className="text-danger"
                                    />
                                  </div>
                                  <div className="form-group col-2">
                                    <label>Action</label>
                                    <br />
                                    <button
                                      type="button"
                                      className="btn btn-outline-danger"
                                      onClick={() => arrayHelpers.remove(index)}
                                    >
                                      Remove
                                    </button>
                                  </div>
                                </div>
                                <br />
                                <br />
                              </div>
                            ))
                          : null}
                      </div>
                    );
                  }}
                />
                <div
                  className="d-flex flex-wrap justify-content-evenly mb-3 mt-3"
                  role="group"
                  aria-label="Basic example"
                >
                  <button
                    type="button"
                    className="btn btn-outline-danger"
                    onClick={() => props.prev(values)}
                  >
                    Back
                  </button>
                  <button type="submit" className="btn btn-outline-primary">
                    Next
                  </button>
                </div>
              </Form>
            )}
          </Formik>
        </Card.Body>
      </Card>
    </div>
  );
};

// Work Experience
const stepThreeValidationSchema = Yup.object({
  workExp: Yup.array()
    .of(
      Yup.object().shape({
        companyName: Yup.string().required("Company Name is a required"),
        work_designation: Yup.string().required(
          "Work Designation is a required"
        ),
        from_date: Yup.date().required("Required").max(new Date(Date.now() -86400000), "Date cannot be in the Today or Future"),
        to_date: Yup.date().required("Required").min(Yup.ref('from_date'),"End date can't be less than From date").max(new Date(Date.now() -86400000), "Date cannot be in the Today or Future"),
      })
    )
    .min(1, "Must have at least Work Experience Details add."),
});

const StepThree = (props) => {
  const handleSubmit = (values) => {
    props.next(values, true);
  };

  return (
    <div className="container">
      <Card style={{ marginTop: 50 }}>
        <Card.Body>
          <Formik
            validationSchema={stepThreeValidationSchema}
            initialValues={props.data}
            onSubmit={handleSubmit}
          >
            {({ values, errors }) => (
              <Form>
                <div className="text-danger text-center">
                  {typeof errors.workExp === "string"
                    ? "Must have at least Work Experience Details add"
                    : null}
                </div>
                <FieldArray
                  name="workExp"
                  render={(arrayHelpers) => {
                    const workExp = values.workExp;
                    return (
                      <div>
                        <div className="mb-3 mt-3 col-11 d-flex justify-content-end">
                          <button
                            type="button"
                            className="btn btn-outline-primary"
                            onClick={() =>
                              arrayHelpers.push({
                                companyName: "",
                                work_designation: "",
                                from_date: "",
                                to_date: "",
                              })
                            }
                          >
                            Add
                          </button>
                        </div>
                        {workExp && workExp.length > 0
                          ? workExp.map((work, index) => (
                              <div key={index}>
                                <div className="row mb-3">
                                  <div className="form-group col-2">
                                    <label>Company Name</label>
                                    <Field
                                      name={`workExp.${index}.companyName`}
                                      type="text"
                                      className="form-control"
                                    />
                                    <ErrorMessage
                                      name={`workExp.${index}.companyName`}
                                      component="div"
                                      className="text-danger"
                                    />
                                  </div>
                                  <div className="form-group col-2">
                                    <label>Work Designation</label>
                                    <Field
                                      name={`workExp.${index}.work_designation`}
                                      type="text"
                                      className="form-control"
                                    />
                                    <ErrorMessage
                                      name={`workExp.${index}.work_designation`}
                                      component="div"
                                      className="text-danger"
                                    />
                                  </div>
                                  <div className="form-group col-2">
                                    <label>From Date</label>
                                    <Field
                                      name={`workExp.${index}.from_date`}
                                      type="date"
                                      className="form-control"
                                    />
                                    <ErrorMessage
                                      name={`workExp.${index}.from_date`}
                                      component="div"
                                      className="text-danger"
                                    />
                                  </div>
                                  <div className="form-group col-2">
                                    <label>To Date</label>
                                    <Field
                                      name={`workExp.${index}.to_date`}
                                      type="date"
                                      className="form-control"
                                    />
                                    <ErrorMessage
                                      name={`workExp.${index}.to_date`}
                                      component="div"
                                      className="text-danger"
                                    />
                                  </div>
                                  <div className="form-group col-2">
                                    <label>Action</label>
                                    <br />
                                    <button
                                      type="button"
                                      className="btn btn-outline-danger"
                                      onClick={() => arrayHelpers.remove(index)}
                                    >
                                      Remove
                                    </button>
                                  </div>
                                </div>
                                <br />
                                <br />
                              </div>
                            ))
                          : null}
                      </div>
                    );
                  }}
                />
                <div
                  className="d-flex flex-wrap justify-content-evenly mb-3 mt-3"
                  role="group"
                  aria-label="Basic example"
                >
                  <button
                    type="button"
                    className="btn btn-outline-danger"
                    onClick={() => props.prev(values)}
                  >
                    Back
                  </button>
                  <button type="submit" className="btn btn-outline-primary">
                    Next
                  </button>
                </div>
              </Form>
            )}
          </Formik>
        </Card.Body>
      </Card>
    </div>
  );
};

// Language KNown
const stepFourValidationSchema = Yup.object({
  langKnow: Yup.array()
    .of(
      Yup.object().shape({
        known: Yup.string().required("Select language"),
        status: Yup.array().min(
          1,
          "Must have at least one language known select"
        ),
      })
    )
    .min(1, "Must have at least one Language Add."),
});

const StepFour = (props) => {
  const handleSubmit = (values) => {
    props.next(values, true);
  };

  return (
    <div className="container">
      <Card style={{ marginTop: 50 }}>
        <Card.Body>
          <Formik
            validationSchema={stepFourValidationSchema}
            initialValues={props.data}
            onSubmit={handleSubmit}
          >
            {({ values, errors }) => (
              <Form>
                <div className="text-danger text-center">
                  {typeof errors.langKnow === "string"
                    ? "Must have at least Language Details add"
                    : null}
                </div>
                <FieldArray
                  name="langKnow"
                  render={(arrayHelpers) => {
                    const langKnow = values.langKnow;
                    return (
                      <div>
                        <div className="mb-3 mt-3 col-11 d-flex justify-content-end">
                          <button
                            type="button"
                            className="btn btn-outline-primary"
                            onClick={() =>
                              arrayHelpers.push({
                                known: "",
                                status: [],
                              })
                            }
                          >
                            Add
                          </button>
                        </div>
                        {langKnow && langKnow.length > 0
                          ? langKnow.map((lang, index) => (
                              <div key={index}>
                                <div className="row mb-3">
                                  <div className="form-group col-4">
                                    <label>Language known</label>
                                    <Field
                                      name={`langKnow.${index}.known`}
                                      className="form-select"
                                      as="select"
                                    >
                                      <option value="">Select Language</option>
                                      <option value="hindi">hindi</option>
                                      <option value="english">english</option>
                                      <option value="gujarati">gujarati</option>
                                    </Field>
                                    <ErrorMessage
                                      name={`langKnow.${index}.known`}
                                      component="div"
                                      className="text-danger"
                                    />
                                  </div>

                                  <div className="form-group col-4">
                                    <label>known</label>
                                    <div
                                      className="d-flex flex-wrap justify-content-between mb-3 mt-2"
                                      role="group"
                                      aria-labelledby="checkbox-group"
                                    >
                                      <label>
                                        <Field
                                          type="checkbox"
                                          name={`langKnow.${index}.status`}
                                          value="read"
                                        />
                                        read
                                      </label>
                                      <label>
                                        <Field
                                          type="checkbox"
                                          name={`langKnow.${index}.status`}
                                          value="write"
                                        />
                                        write
                                      </label>
                                      <label>
                                        <Field
                                          type="checkbox"
                                          name={`langKnow.${index}.status`}
                                          value="speak"
                                        />
                                        speak
                                      </label>
                                    </div>
                                    <ErrorMessage
                                      name={`langKnow.${index}.status`}
                                      component="div"
                                      className="text-danger"
                                    />
                                  </div>

                                  <div className="form-group col-4">
                                    <label>Action</label>
                                    <br />
                                    <button
                                      type="button"
                                      className="btn btn-outline-danger"
                                      onClick={() => arrayHelpers.remove(index)}
                                    >
                                      Remove
                                    </button>
                                  </div>
                                </div>
                              </div>
                            ))
                          : null}
                      </div>
                    );
                  }}
                />
                <div
                  className="d-flex flex-wrap justify-content-evenly mb-3 mt-3"
                  role="group"
                  aria-label="Basic example"
                >
                  <button
                    type="button"
                    className="btn btn-outline-danger"
                    onClick={() => props.prev(values)}
                  >
                    Back
                  </button>
                  <button type="submit" className="btn btn-outline-primary">
                    Next
                  </button>
                </div>
              </Form>
            )}
          </Formik>
        </Card.Body>
      </Card>
    </div>
  );
};

// Technologies youo know
const stepFiveValidationSchema = Yup.object({
  techKnow: Yup.array()
    .of(
      Yup.object().shape({
        known: Yup.string().required("Select Technology"),
        status: Yup.string().required("Select Status"),
      })
    )
    .min(1, "Must have at least one Technology Add."),
});

const StepFive = (props) => {
  const handleSubmit = (values) => {
    props.next(values, true);
  };

  return (
    <div className="container">
      <Card style={{ marginTop: 50 }}>
        <Card.Body>
          <Formik
            validationSchema={stepFiveValidationSchema}
            initialValues={props.data}
            onSubmit={handleSubmit}
          >
            {({ values, errors }) => (
              <Form>
                <div className="text-danger text-center">
                  {typeof errors.techKnow === "string"
                    ? "Must have at least Technology Details add"
                    : null}
                </div>
                <FieldArray
                  name="techKnow"
                  render={(arrayHelpers) => {
                    const techKnow = values.techKnow;
                    return (
                      <div>
                        <div className="mb-3 mt-3 col-11 d-flex justify-content-end">
                          <button
                            type="button"
                            className="btn btn-outline-primary"
                            onClick={() =>
                              arrayHelpers.push({
                                known: "",
                                status: "",
                              })
                            }
                          >
                            Add
                          </button>
                        </div>
                        {techKnow && techKnow.length > 0
                          ? techKnow.map((tech, index) => (
                              <div key={index}>
                                <div className="row mb-3">
                                  <div className="form-group col-4">
                                    <label>Technologies known</label>
                                    <Field
                                      name={`techKnow.${index}.known`}
                                      className="form-select"
                                      as="select"
                                    >
                                      <option value="">
                                        Select Technologies
                                      </option>
                                      <option value="php">php</option>
                                      <option value="mysql">mysql</option>
                                      <option value="laravel">laravel</option>
                                      <option value="oracle">oracle</option>
                                    </Field>
                                    <ErrorMessage
                                      name={`techKnow.${index}.known`}
                                      component="div"
                                      className="text-danger"
                                    />
                                  </div>

                                  <div className="form-group col-4">
                                    <label>Status</label>
                                    <div
                                      className="d-flex flex-wrap justify-content-between mb-3 mt-2"
                                      role="group"
                                      aria-labelledby="my-radio-group"
                                    >
                                      <label>
                                        <Field
                                          type="radio"
                                          name={`techKnow.${index}.status`}
                                          value="beginer"
                                        />
                                        beginer
                                      </label>
                                      <label>
                                        <Field
                                          type="radio"
                                          name={`techKnow.${index}.status`}
                                          value="mideator"
                                        />
                                        mideator
                                      </label>
                                      <label>
                                        <Field
                                          type="radio"
                                          name={`techKnow.${index}.status`}
                                          value="expert"
                                        />
                                        expert
                                      </label>
                                    </div>
                                    <ErrorMessage
                                      name={`techKnow.${index}.status`}
                                      component="div"
                                      className="text-danger"
                                    />
                                  </div>

                                  <div className="form-group col-4">
                                    <label>Action</label>
                                    <br />
                                    <button
                                      type="button"
                                      className="btn btn-outline-danger"
                                      onClick={() => arrayHelpers.remove(index)}
                                    >
                                      Remove
                                    </button>
                                  </div>
                                </div>
                              </div>
                            ))
                          : null}
                      </div>
                    );
                  }}
                />
                <div
                  className="d-flex flex-wrap justify-content-evenly mb-3 mt-3"
                  role="group"
                  aria-label="Basic example"
                >
                  <button
                    type="button"
                    className="btn btn-outline-danger"
                    onClick={() => props.prev(values)}
                  >
                    Back
                  </button>
                  <button type="submit" className="btn btn-outline-primary">
                    Next
                  </button>
                </div>
              </Form>
            )}
          </Formik>
        </Card.Body>
      </Card>
    </div>
  );
};

// Referance Contact
const stepSixValidationSchema = Yup.object({
  refCont: Yup.array()
    .of(
      Yup.object().shape({
        ref_name: Yup.string().required("Reference Name is a required"),
        ref_number: Yup.string()
          .matches(/^[0-9\b]+$/, "allow only number")
          .length(10, "Reference Number must be exactly 10 characters")
          .required("Reference Number is a required"),
        ref_relation: Yup.string().required("Reference Relation is a required"),
      })
    )
    .min(1, "Must have at least Reference Details add."),
});

const StepSix = (props) => {
  const handleSubmit = (values) => {
    props.next(values, true);
  };

  return (
    <div className="container">
      <Card style={{ marginTop: 50 }}>
        <Card.Body>
          <Formik
            validationSchema={stepSixValidationSchema}
            initialValues={props.data}
            onSubmit={handleSubmit}
          >
            {({ values, errors }) => (
              <Form>
                <div className="text-danger text-center">
                  {typeof errors.refCont === "string"
                    ? "Must have at least Reference Details add"
                    : null}
                </div>
                <FieldArray
                  name="refCont"
                  render={(arrayHelpers) => {
                    const refCont = values.refCont;
                    return (
                      <div>
                        <div className="mb-3 mt-3 col-11 d-flex justify-content-end">
                          <button
                            type="button"
                            className="btn btn-outline-primary"
                            onClick={() =>
                              arrayHelpers.push({
                                ref_name: "",
                                ref_number: "",
                                ref_relation: "",
                              })
                            }
                          >
                            Add
                          </button>
                        </div>
                        {refCont && refCont.length > 0
                          ? refCont.map((ref, index) => (
                              <div key={index}>
                                <div className="row mb-3">
                                  <div className="form-group col-3">
                                    <label>Name</label>
                                    <Field
                                      name={`refCont.${index}.ref_name`}
                                      type="text"
                                      className="form-control"
                                    />
                                    <ErrorMessage
                                      name={`refCont.${index}.ref_name`}
                                      component="div"
                                      className="text-danger"
                                    />
                                  </div>
                                  <div className="form-group col-3">
                                    <label>Number</label>
                                    <Field
                                      name={`refCont.${index}.ref_number`}
                                      type="text"
                                      className="form-control"
                                    />
                                    <ErrorMessage
                                      name={`refCont.${index}.ref_number`}
                                      component="div"
                                      className="text-danger"
                                    />
                                  </div>
                                  <div className="form-group col-3">
                                    <label>Relation</label>
                                    <Field
                                      name={`refCont.${index}.ref_relation`}
                                      type="text"
                                      className="form-control"
                                    />
                                    <ErrorMessage
                                      name={`refCont.${index}.ref_relation`}
                                      component="div"
                                      className="text-danger"
                                    />
                                  </div>
                                  <div className="form-group col-3">
                                    <label>Action</label>
                                    <br />
                                    <button
                                      type="button"
                                      className="btn btn-outline-danger"
                                      onClick={() => arrayHelpers.remove(index)}
                                    >
                                      Remove
                                    </button>
                                  </div>
                                </div>
                                <br />
                                <br />
                              </div>
                            ))
                          : null}
                      </div>
                    );
                  }}
                />
                <div
                  className="d-flex flex-wrap justify-content-evenly mb-3 mt-3"
                  role="group"
                  aria-label="Basic example"
                >
                  <button
                    type="button"
                    className="btn btn-outline-danger"
                    onClick={() => props.prev(values)}
                  >
                    Back
                  </button>
                  <button type="submit" className="btn btn-outline-primary">
                    Next
                  </button>
                </div>
              </Form>
            )}
          </Formik>
        </Card.Body>
      </Card>
    </div>
  );
};

// Preferances
const stepSevenValidationSchema = Yup.object({
  notice_period: Yup.string().required("Notice Period is a required"),
  expected_ctc: Yup.string()
    .matches(/^[0-9\b]+$/, "allow only number")
    .required("Expected CTC is a required"),
  current_ctc: Yup.string()
    .matches(/^[0-9\b]+$/, "allow only number")
    .required("Current CTC is a required"),
  preferd_loc: Yup.array().required("Select Preferd Location"),
  department: Yup.string().required("Select Department"),
});

const StepSeven = (props) => {
  const handleSubmit = (values) => {
    props.next(values, true);
  };

  return (
    <div className="container">
      <Card style={{ marginTop: 50 }}>
        <Card.Body>
          <Formik
            validationSchema={stepSevenValidationSchema}
            initialValues={props.data}
            onSubmit={handleSubmit}
          >
            {({ values }) => (
              <Form>
                <div className="row mb-3">
                  <div className="form-group col-4">
                    <label>Notice Period</label>
                    <Field
                      name="notice_period"
                      type="text"
                      className="form-control"
                    />
                    <ErrorMessage
                      name="notice_period"
                      component="div"
                      className="text-danger"
                    />
                  </div>

                  <div className="form-group col-4">
                    <label>Expected CTC</label>
                    <Field
                      name="expected_ctc"
                      type="text"
                      className="form-control"
                    />
                    <ErrorMessage
                      name="expected_ctc"
                      component="div"
                      className="text-danger"
                    />
                  </div>

                  <div className="form-group col-4">
                    <label>Current CTC</label>
                    <Field
                      name="current_ctc"
                      type="text"
                      className="form-control"
                    />
                    <ErrorMessage
                      name="current_ctc"
                      component="div"
                      className="text-danger"
                    />
                  </div>
                </div>

                <div className="row mb-3">
                  <div className="form-group col-4">
                    <label>Preferred Location</label>
                    <Field
                      name="preferd_loc"
                      className="form-select"
                      as="select"
                      multiple
                    >
                      <option value="" disabled>
                        Select location
                      </option>
                      <option value="ahmedabad">Ahmedabad</option>
                      <option value="bhavnagar">Bhavnagar</option>
                      <option value="rajkot">Rajkot</option>
                      <option value="vadodara">Vadodara</option>
                    </Field>
                    <ErrorMessage
                      name="preferd_loc"
                      component="div"
                      className="text-danger"
                    />
                  </div>

                  <div className="form-group col-4">
                    <label>Department</label>
                    <Field
                      name="department"
                      className="form-select"
                      as="select"
                    >
                      <option value="">Select department</option>
                      <option value="design">Design</option>
                      <option value="marketing">marketing</option>
                    </Field>
                    <ErrorMessage
                      name="department"
                      component="div"
                      className="text-danger"
                    />
                  </div>
                </div>

                <div
                  className="d-flex flex-wrap justify-content-evenly mb-3 mt-3"
                  role="group"
                  aria-label="Basic example"
                >
                  <button
                    type="button"
                    className="btn btn-outline-danger"
                    onClick={() => props.prev(values)}
                  >
                    Back
                  </button>
                  <button type="submit" className="btn btn-outline-primary">
                    Submit
                  </button>
                </div>
              </Form>
            )}
          </Formik>
        </Card.Body>
      </Card>
    </div>
  );
};