import { useState } from "react";
import { Card } from "react-bootstrap";
import { Formik, Form } from "formik";
import * as Yup from "yup";
import FormikComponent from "./formikComponent";

const JobAppForm = () => {
  let validationValue = [];
  const [step, setstep] = useState(1);
  let [isboolvalue, setisboolvalue] = useState(false);
  let [isbutton, setisbutton] = useState(false);

  if (step === 1) {
    validationValue = Yup.object().shape({
      firstName: Yup.string().required(),
      lastName: Yup.string().required(),
      email: Yup.string().email("Invalid email").required("Required"),
      designation: Yup.string().required(),
      phone: Yup.string()
        .matches(/^[0-9\b]+$/, "allow only number")
        .length(10)
        .required(),
      dob: Yup.date()
        .max(
          new Date(Date.now() - 567648000000),
          "You must be at least 18 years"
        )
        .required("Date of Birth is Required"),
      add1: Yup.string().required("Address Is Required"),
      add2: Yup.string().nullable(true),
      country: Yup.string().required("Select Country"),
      state: Yup.string().required("Select State"),
      city: Yup.string().required("Select City"),
      zip: Yup.string()
        .matches(/^[0-9\b]+$/, "allow only number")
        .length(6)
        .required(),
      gender: Yup.string().required("Select Gender"),
      relStatus: Yup.string().required("RelationShip Status is a required"),
    });
  } else if (step === 2) {
    validationValue = Yup.object().shape({
      education: Yup.array()
        .of(
          Yup.object().shape({
            result: Yup.string().required("Select Result"),
            board: Yup.string().required("Board is a required"),
            school_university: Yup.string().required(
              "school / university is a required"
            ),
            passing_year: Yup.string()
              .matches(/^[0-9\b]+$/, "allow only number")
              .min(2, "Passing Year must be at least 2 characters")
              .max(4, "Passing Year must be at least 4 characters")
              .required("Passing Year is a required"),
            percentage: Yup.string()
              .matches(/^[0-9\b]+$/, "allow only number")
              .max(3, "percentage must be at least 3 characters")
              .required("Passing Year is a required")
              .required("Percentage is a required"),
          })
        )
        .min(1, "Must have at least Education Details add.")
        .required(),
    });
  } else if (step === 3) {
    validationValue = Yup.object().shape({
      workExp: Yup.array()
        .of(
          Yup.object().shape({
            companyName: Yup.string().required("Company Name is a required"),
            work_designation: Yup.string().required(
              "Work Designation is a required"
            ),
            from_date: Yup.date()
              .required("Required")
              .max(
                new Date(Date.now() - 86400000),
                "Date cannot be in the Today or Future"
              ),
            to_date: Yup.date()
              .required("Required")
              .min(
                Yup.ref("from_date"),
                "End date can't be less than From date"
              )
              .max(
                new Date(Date.now() - 86400000),
                "Date cannot be in the Today or Future"
              ),
          })
        )
        .min(1, "Must have at least Work Experience Details add."),
    });
  } else if (step === 4) {
    validationValue = Yup.object().shape({
      langKnow: Yup.array()
        .of(
          Yup.object().shape({
            known: Yup.string().required("Select language"),
            status: Yup.array().min(
              1,
              "Must have at least one language known select"
            ),
          })
        )
        .min(1, "Must have at least one Language Add."),
    });
  } else if (step === 5) {
    validationValue = Yup.object().shape({
      techKnow: Yup.array()
        .of(
          Yup.object().shape({
            known: Yup.string().required("Select Technology"),
            status: Yup.string().required("Select Status"),
          })
        )
        .min(1, "Must have at least one Technology Add."),
    });
  } else if (step === 6) {
    validationValue = Yup.object().shape({
      refCont: Yup.array()
        .of(
          Yup.object().shape({
            ref_name: Yup.string().required("Reference Name is a required"),
            ref_number: Yup.string()
              .matches(/^[0-9\b]+$/, "allow only number")
              .length(10, "Reference Number must be exactly 10 characters")
              .required("Reference Number is a required"),
            ref_relation: Yup.string().required(
              "Reference Relation is a required"
            ),
          })
        )
        .min(1, "Must have at least Reference Details add."),
    });
  } else {
    validationValue = Yup.object().shape({
      notice_period: Yup.string().required("Notice Period is a required"),
      expected_ctc: Yup.string()
        .matches(/^[0-9\b]+$/, "allow only number")
        .required("Expected CTC is a required"),
      current_ctc: Yup.string()
        .matches(/^[0-9\b]+$/, "allow only number")
        .required("Current CTC is a required"),
      preferd_loc: Yup.array().of(Yup.string().required()
      ).min(1,"Select One Location"),
      department: Yup.string().required("Select Department"),
    });
  }

  const nextStep = (formik) => {
    setisboolvalue(true);

    if (Object.keys(formik.errors).length === 0) {
      setstep(step + 1);
      setisbutton(false);
    }
  };
  const prevStep = (formik) => {
    setisboolvalue(true);
    if (Object.keys(formik.errors).length === 0) {
      setstep(step - 1);
      setisbutton(false);
    }
  };

  return (
    <div className="container">
      <Card style={{ marginTop: 50 }}>
        <Card.Body>
          <Formik
            initialValues={{
              firstName: "",
              lastName: "",
              email: "",
              designation: "",
              phone: "",
              dob: "",
              add1: "",
              add2: "",
              country: "",
              state: "",
              city: "",
              zip: "",
              gender: "",
              relStatus: "",
              education: [],
              workExp: [],
              langKnow: [],
              techKnow: [],
              refCont: [],
              notice_period: "",
              expected_ctc: "",
              current_ctc: "",
              preferd_loc: [],
              department: "",
            }}
            validationSchema={validationValue}
            onSubmit={(values) => {
              setisboolvalue(true);
              setisbutton(true);
              console.log(values);
            }}
          >
            {(formik) => {
              console.log(formik.errors);

              return (
                <Form>
                  <FormikComponent
                    controller={step}
                    formik={formik}
                    isboolvalue={isboolvalue}
                    setisbutton={setisbutton}
                  />
                  <div
                    className="d-flex flex-wrap justify-content-evenly mb-3 mt-3"
                    role="group"
                    aria-label="Basic example"
                  >
                    {step !== 1 ? (
                      <button
                        type="button"
                        className="btn btn-outline-danger"
                        onClick={() => prevStep(formik)}
                      >
                        Previous
                      </button>
                    ) : null}
                    {step === 7 ? (
                      <button className="btn btn-outline-primary" type="submit">
                        Submit
                      </button>
                    ) : (
                      <button
                        type="button"
                        className="btn btn-outline-primary"
                        onClick={() => nextStep(formik)}
                        disabled={!isbutton}
                      >
                        Next
                      </button>
                    )}
                  </div>
                </Form>
              );
            }}
          </Formik>
        </Card.Body>
      </Card>
    </div>
  );
};

export default JobAppForm;
