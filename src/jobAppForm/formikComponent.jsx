import BasicDetails from "../components/basicDetails";
import EducationDetail from "../components/educationDetail";
import WorkExperience from "../components/workExperience";
import LanguageKnown from "../components/languageKnown";
import TechnologiesKnown from "../components/technologiesKnown";
import ReferenceContact from "../components/referenceContact";
import Preferences from "../components/preferences";

const FormikComponent = ({ controller, formik,isboolvalue,setisbutton }) => {
  switch (controller) {
    case 1:
      return (
        <BasicDetails
        formik={formik} isboolvalue={isboolvalue} setisbutton={setisbutton}
        />
      );
    case 2:
      return (
        <EducationDetail
        formik={formik} isboolvalue={isboolvalue} setisbutton={setisbutton}
        />
      );
    case 3:
      return (
        <WorkExperience
        formik={formik} isboolvalue={isboolvalue} setisbutton={setisbutton}
        />
      );
    case 4:
      return (
        <LanguageKnown
        formik={formik} isboolvalue={isboolvalue} setisbutton={setisbutton}
        />
      );
    case 5:
      return (
        <TechnologiesKnown
        formik={formik} isboolvalue={isboolvalue} setisbutton={setisbutton}
        />
      );
    case 6:
      return (
        <ReferenceContact
        formik={formik} isboolvalue={isboolvalue} setisbutton={setisbutton}
        />
      );
    case 7:
      return (
        <Preferences
        formik={formik} isboolvalue={isboolvalue} setisbutton={setisbutton}
        />
      );
    default:
      return;
  }
};

export default FormikComponent;
